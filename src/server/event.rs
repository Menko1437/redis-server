use std::{
    borrow::BorrowMut,
    sync::{Arc, Mutex},
    time::Duration,
};

use super::cache;

#[allow(dead_code)]
const SIMPLE_STRING: char = '+';
#[allow(dead_code)]
const ERROR: char = '-';
#[allow(dead_code)]
const INTEGER: char = ':';
const BULK: char = '$';
const ARRAY: char = '*';
const TERM: char = '\0';

#[derive(Clone, PartialEq)]
pub enum EventType {
    RespEcho,
    Unknown,
    None,
}

#[derive(Clone)]
pub struct Event {
    pub event_type: EventType,
    pub command: String,
    pub args: Vec<String>,
    pub complete: bool,
}

impl<'a> Event {
    pub fn execute(&self, cache: Arc<Mutex<cache::Cache<String, String>>>) -> String {
        match self.command.as_str() {
            "ECHO" | "echo" => format!("{}{}", "+", self.args[1..].join(" ")),
            "PING" | "ping" => {
                if self.args.len() > 1 {
                    format!("{}{}", "+", self.args[1..].join(" "))
                } else {
                    format!("{}{}", "+", "PONG".to_string())
                }
            }
            "SET" | "set" => {
                let parsed_set = parse_set_event(&self);
                cache.lock().unwrap().borrow_mut().write(
                    parsed_set.0,
                    parsed_set.1.trim_end().to_string(),
                    parsed_set.2,
                );

                format!("{}{}", "+", "OK".to_string())
            }
            "GET" | "get" => {
                let value = match cache.lock().unwrap().read(&self.args[1]) {
                    Some(value) => format!("{}{}", "+", value),
                    None => format!("{}{}", "$-", "1".to_string()),
                };

                value
            }
            _ => format!("{}{}", "+", "OK".to_owned()),
        }
    }
}

pub fn build_empty_event(event_type: EventType) -> Event {
    Event {
        event_type: event_type,
        command: String::new(),
        args: Vec::new(),
        complete: false,
    }
}

pub fn detect_event_type(message: &String) -> EventType {
    if message.contains("*") || message.contains("$") {
        return EventType::RespEcho;
    }

    EventType::Unknown
}

pub fn parse_set_event(event: &Event) -> (String, String, Option<Duration>) {
    let key = event.args[1].clone();
    let mut message = String::new();
    let mut expire: Option<Duration> = None;

    let mut args_iter = event.args.iter().skip(2);
    while let Some(arg) = args_iter.next() {
        match arg {
            a if a == &String::from("px") => {
                expire = Some(Duration::from_millis(
                    args_iter.next().unwrap().parse().unwrap(),
                ));
            }
            _ => message.push_str(format!("{} ", arg).as_str()),
        }
    }
    {}

    (key, message, expire)
}

pub fn generate_resp_event_batch(message: String, prev_skip: &mut Vec<Event>) -> Vec<Event> {
    let mut events: Vec<Event> = Vec::new();

    let mut msg_chars = message.chars();

    let mut new_event: Event = build_empty_event(EventType::RespEcho);
    let mut nargs: u32 = 0;
    let mut word_len: u32;

    while let Some(c) = msg_chars.next() {
        match c {
            ARRAY => {
                if new_event.args.len() > 0 {
                    new_event.command = new_event.args.first().clone().unwrap().to_owned();
                    events.push(new_event);
                }

                new_event = build_empty_event(EventType::RespEcho);
                nargs = match msg_chars.next() {
                    Some(ch) => ch.to_digit(10).unwrap_or(0),
                    None => {
                        new_event.complete = false;
                        0
                    }
                }
            }
            BULK => {
                word_len = msg_chars.next().unwrap().to_digit(10).unwrap_or(0);

                let mut word = String::new();

                while word_len > 0 {
                    match msg_chars.next() {
                        Some(ch) => word.push(ch),
                        None => {
                            new_event.complete = false;
                        }
                    };
                    word_len = word_len - 1;
                }

                new_event.args.push(word);

                if new_event.args.len() >= nargs as usize {
                    new_event.complete = true;
                }
            }
            TERM => {
                break;
            }
            // skipped in last batch
            _ => {
                let star_pos = match message.find("*") {
                    Some(pos) => pos as i32,
                    None => 0,
                };

                let dolar_pos = match message.find("$") {
                    Some(pos) => pos as i32,
                    None => 0,
                };

                let diff = star_pos - dolar_pos;
                let mut word_len = match diff {
                    star if star > 0 => star_pos,
                    dol if dol < 0 => dolar_pos,
                    _ => message.len() as i32,
                };

                let mut word = String::new();

                while word_len > 0 {
                    match msg_chars.next() {
                        Some(ch) => word.push(ch),
                        None => (),
                    };
                    word_len = word_len - 1;
                }

                match prev_skip.last_mut() {
                    Some(event) => {
                        match event.args.last_mut() {
                            Some(s) => {
                                s.push_str(&word);
                                events.push(event.clone());
                            }
                            None => (),
                        };
                    }
                    None => (),
                }
            }
        }
    }

    if new_event.args.len() > 0 {
        new_event.command = new_event.args.first().clone().unwrap().to_owned();
    }
    events.push(new_event);

    events
}
