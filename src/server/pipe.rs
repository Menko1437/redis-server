use super::event;
use super::event::EventType;
use super::util;

pub struct EventPipe {
    pub pipe: Vec<event::Event>,
    pub skiped_queue: Vec<event::Event>,
    last_event_type: EventType,
}

impl<'a> EventPipe {
    pub fn new() -> Self {
        EventPipe {
            pipe: Vec::new(),
            skiped_queue: Vec::new(),
            last_event_type: EventType::None,
        }
    }

    pub fn insert(&mut self, message: String) {
        let message = util::remove_newline(&message);

        let event_type = event::detect_event_type(&message);

        match_event(event_type, self, message);
    }

    pub fn fetch(&self) -> &Vec<event::Event> {
        &self.pipe
    }

    pub fn clear(&mut self) {
        self.pipe = Vec::new();
        self.skiped_queue = Vec::new();
    }
}

fn match_event(event_type: EventType, event_pipe: &mut EventPipe, message: String) {
    match event_type {
        event::EventType::RespEcho => {
            let event_batch =
                event::generate_resp_event_batch(message, &mut event_pipe.skiped_queue);
            for event in event_batch {
                if event.complete {
                    event_pipe.pipe.push(event);
                } else {
                    event_pipe.skiped_queue.push(event);
                }
            }
        }
        event::EventType::Unknown => {
            if event_pipe.last_event_type != event::EventType::None {
                match_event(event_pipe.last_event_type.clone(), event_pipe, message);
            }
        }
        event::EventType::None => (),
    }
}
