pub fn remove_newline(s: &String) -> String {
    let mut output: String = String::from(s);

    while let Some(p) = output.find("\n") {
        output.remove(p);
    }

    while let Some(p) = output.find("\r") {
        output.remove(p);
    }

    output
}
