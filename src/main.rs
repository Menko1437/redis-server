mod server;

const SERVER_ADDRESS: &str = "127.0.0.1:6379";

fn main() -> std::io::Result<()> {
    let server_handle = std::thread::spawn(|| {
        match server::server::connect(SERVER_ADDRESS) {
            Ok(l) => {
                server::server::poll(&l)?;
                drop(l);
            }
            Err(e) => {
                println!("[Server] Error: {}", e);
                return Err(e);
            }
        };
        Ok(())
    });

    server_handle.join().unwrap().unwrap();

    Ok(())
}
