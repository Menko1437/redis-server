use std::borrow::Borrow;
use std::borrow::BorrowMut;
use std::io::Read;
use std::io::Result;
use std::io::Write;
use std::net::TcpListener;
use std::net::TcpStream;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread::JoinHandle;

use super::cache;
use super::pipe;

pub fn connect(addr: &str) -> Result<TcpListener> {
    match TcpListener::bind(addr) {
        Ok(listener) => return Ok(listener),
        Err(e) => return Err(e),
    };
}

pub fn poll(listener: &TcpListener) -> Result<()> {
    let mut handles: Vec<JoinHandle<bool>> = Vec::new();
    let cache: Arc<Mutex<cache::Cache<String, String>>> = Arc::new(Mutex::new(cache::Cache::new()));

    loop {
        let incoming = listener.incoming();
        let threaded_cache = Arc::clone(&cache);

        let handle = std::thread::spawn(|| check_cache(threaded_cache));
        handles.push(handle);

        for connection in incoming.into_iter() {
            let connection = connection.unwrap();
            let threaded_cache_inner = Arc::clone(&cache);
            let handle = std::thread::spawn(|| handle_clinet(connection, threaded_cache_inner));
            handles.push(handle);
        }

        for h in handles {
            h.join().unwrap();
        }
        return Ok(());
    }
}

pub fn check_cache(cache: Arc<Mutex<cache::Cache<String, String>>>) -> bool {
    loop {
        cache.lock().unwrap().borrow_mut().check_for_expired();

        if let Ok(c) = cache.try_lock() {
            if c.borrow().should_close() {
                break;
            }
        }
    }
    true
}

pub fn handle_clinet(
    mut stream: TcpStream,
    cache: Arc<Mutex<cache::Cache<String, String>>>,
) -> bool {
    let mut buf = [0 as u8; 512];
    let mut pipe = pipe::EventPipe::new();

    loop {
        while match stream.read(&mut buf) {
            Ok(_) => {
                if buf[0] == 0 {
                    return true;
                }

                let message = String::from_utf8(buf.to_vec()).unwrap();
                buf = [0 as u8; 512];

                if let Ok(_) = stream.flush() {}

                pipe.insert(message);

                for event in pipe.fetch() {
                    let shared_cache = Arc::clone(&cache);
                    let event_output = event.execute(shared_cache);

                    if event_output.len() > 0 {
                        match stream.write(format!("{}\r\n", event_output).as_bytes()) {
                            Ok(_) => (),
                            Err(e) => println!("[Server] Failed to write message. {}", e),
                        }
                    }
                }
                pipe.clear();
                true
            }
            Err(_) => {
                if let Ok(_) = stream.shutdown(std::net::Shutdown::Both) {
                    println!("[Server] Stream shutdown");
                }
                cache.lock().unwrap().signal_cache_close();
                return false;
            }
        } {}
    }
}
