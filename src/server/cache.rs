use std::cell::RefCell;
use std::collections::HashMap;
use std::hash::Hash;
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};

#[derive(Debug, Clone, Copy)]
struct Node<K, V> {
    key: K,
    value: V,
    time: std::time::Instant,
    expiry: Option<Duration>,
}

impl<K, V> Node<K, V> {
    fn new(key: K, value: V, expiry: Option<Duration>) -> Box<Self> {
        Box::new(Node {
            key,
            value,
            time: std::time::Instant::now(),
            expiry,
        })
    }
}

#[derive(Debug)]
pub struct Cache<K, V>
where
    K: Hash + Eq + Clone,
    V: Clone,
{
    map: Arc<Mutex<RefCell<HashMap<K, Node<K, V>>>>>,
    should_close: bool,
}

impl<K, V> Cache<K, V>
where
    K: Hash + Eq + Clone,
    V: Clone,
{
    pub fn new() -> Self {
        Cache {
            map: Arc::new(Mutex::new(RefCell::new(HashMap::new()))),
            should_close: false,
        }
    }

    pub fn write(&mut self, key: K, value: V, expiry: Option<Duration>) {
        self.delete(&key);

        let node = Node::new(key.clone(), value, expiry);

        let mut lock = self.map.lock();

        if let Ok(ref mut map) = lock {
            if let Ok(mut map) = map.try_borrow_mut() {
                map.insert(key, *node);
            }
        }
    }

    pub fn read(&self, key: &K) -> Option<V> {
        let node = self
            .map
            .lock()
            .unwrap()
            .try_borrow_mut()
            .unwrap()
            .get_mut(key)
            .map(|node| node.clone())?;

        Some(node.value)
    }

    pub fn delete(&mut self, key: &K) -> Option<V> {
        let node = self
            .map
            .lock()
            .unwrap()
            .try_borrow_mut()
            .unwrap()
            .remove(&key)?;

        Some(node.value)
    }

    pub fn clear(&self) {
        self.map
            .lock()
            .unwrap()
            .try_borrow_mut()
            .unwrap()
            .drain()
            .for_each(|(_, node)| {
                drop(node);
            });
    }

    pub fn signal_cache_close(&mut self) {
        self.should_close = true;
    }

    pub fn should_close(&self) -> bool {
        self.should_close
    }

    pub fn check_for_expired(&mut self) {
        self.map
            .lock()
            .unwrap()
            .try_borrow_mut()
            .unwrap()
            .retain(|_, node| {
                if let None = node.expiry {
                    return true;
                }
                let now = Instant::now();
                if now > node.time && now - node.time > node.expiry.unwrap() {
                    return false;
                }

                true
            });
    }
}

impl<K, V> Drop for Cache<K, V>
where
    K: Eq + Hash + Clone,
    V: Clone,
{
    fn drop(&mut self) {
        self.clear();
    }
}
